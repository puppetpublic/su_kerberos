[[_TOC_]]

# `su_kerberos` Puppet Class

## Quick Start

Use this Puppet class to install Kerberos client software and the
Stanford-recommended version of `/etc/krb5.conf`:
```
include su_kerberos
```

## Configuration parameters

* `ensure`: one of "absent" or "present". If "present" will install the
appropriate kerberos client package and install the /etc/krb5.conf file
(unless the `manage_etc_krb5conf` parameter is set to false). If "absent"
will ensure that the appropriate package is NOT present and will remove
`/etc/krb5.conf` (unless the $manage_etc_krb5conf parameter is set to
false). Recommendation: only set to absent if you are absolutely sure you
will not need to use *any* Kerberos-based applications.  Default:
"present".

* `debian_client`: if the server where this Puppet module runs is Debian,
this parameter determines which kerberos client package you will
install. Its value must be one of `mit` or `heimdal`; the default is
`mit`. If the server is not Debian this parameter has no effect.

* `manage_etc_krb5conf`: if set to true (the default) will install the
Stanford-recommended version of `/etc/krb5.conf` by the `krb5conf` Puppet
module. Set this to `false` if you want to manage `/etc/krb5.conf` in some
other class.

## Notes

* Do **not** use this class with a Kerberos server as a Kerberos server
usually requires a highly-customized Kerberos configuration.
