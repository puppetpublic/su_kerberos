# Set up basic Kerberos configuration.

# Do **not** use this class with a Kerberos server as a Kerberos server
# usually requires a highly-customized Kerberos configuration.

# $ensure: one of "absent" or "present". If "present" will install the
# appropriate kerberos client package and install the /etc/krb5.conf file
# (unless the $manage_etc_krb5conf parameter is set to false). If "absent"
# will ensure that the appropriate package is NOT present and will remove
# /etc/krb5.conf (unless the $manage_etc_krb5conf parameter is set to
# false). Default: "present".

# $manage_etc_krb5conf: if set to true (the default) will install the
# Stanford-recommended version of /etc/krb5.conf using the krb5conf Puppet
# module. Set this to false if you want to manage /etc/krb5.conf in some
# other class.

# $debian_client: this determines which kerberos client package you
# will install. Its value must be one of "mit' or "heimdal"; the default
# is "mit'.

class su_kerberos (
  Enum['present', 'absent'] $ensure              = 'present',
  Boolean                   $manage_etc_krb5conf = true,
  Enum['heimdal', 'mit']    $debian_client       = 'mit',
) {

  # Step 1. Install the kerberos client.
  case $::osfamily {
    'RedHat': {
      package { 'krb5-workstation': ensure => $ensure }
    }
    'Debian': {
      case $debian_client {
        'mit':     { package { 'krb5-user':       ensure => $ensure } }
        'heimdal': { package { 'heimdal-clients': ensure => $ensure } }
        default:   { fail("unsupported debian_client value '${debian_client}'") }
      }
    }
    default: {
      fail("unsupported OS ${::operatingsystem}")
    }
  }

  # Step 2. Install the default /etc/krb5.conf file appropriate for
  # Stanford.
  if ($manage_etc_krb5conf) {
    if ($ensure == "present") {
      krb5conf { '/etc/krb5.conf':
        appdefaults_file => 'krb5conf/appdefaults',
      }
    } else {
      file { '/etc/krb5.conf':
        ensure => absent,
      }
    }
  }

}
